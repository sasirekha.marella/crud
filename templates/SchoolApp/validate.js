function validate()
{ 
   if( document.Student-info.textnames.value == "" )
   {
     alert( "Please provide your Name!" );
     document.Student-info.textnames.focus() ;
     return false;
   }
   
  
   if( document.Student-info.Grade.value == "-1" )
   {
     alert( "Please provide your Grade!" );
    
     return false;
   }   
   
    if( document.Student-info.Section.value == "-1" )
   {
     alert( "Please provide your Section!" );
    
     return false;
   }   

var email = document.Student-info.emailid.value;
  atpos = email.indexOf("@");
  dotpos = email.lastIndexOf(".");
 if (email == "" || atpos < 1 || ( dotpos - atpos < 2 )) 
 {
     alert("Please enter correct email ID")
     document.Student-info.emailid.focus() ;
     return false;
 }

  if( document.Student-info.mobileno.value == "" ||
           isNaN( document.Student-info.mobileno.value) ||
           document.Student-info.mobileno.value.length != 10 )
   {
     alert( "Please provide a Mobile No in the format 123." );
     document.Student-info.mobileno.focus() ;
     return false;
   }
   return( true );
}
O